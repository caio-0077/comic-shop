import {
  FC,
  Dispatch,
  useState,
  ReactNode,
  useEffect,
  useContext,
  createContext,
  SetStateAction,
} from 'react';
import { CartItem } from 'types';
import { storage } from 'utils/local-storage';

interface CartContextType {
  itemsInCarts: CartItem[];
  setItemsInCarts: Dispatch<SetStateAction<CartItem[]>>;
}

interface CartProviderProps {
  children: ReactNode;
}

const CartContext = createContext<CartContextType | undefined>(undefined);

export const CartProvider: FC<CartProviderProps> = ({ children }) => {
  const [itemsInCarts, setItemsInCarts] = useState<CartItem[]>([]);
  const itemsInCartsLocalStorage = localStorage.getItem(storage.checkout);
  const itemsInCartsFormattedLocalStorage = JSON.parse(
    itemsInCartsLocalStorage || '[]',
  );

  useEffect(() => {
    if (itemsInCartsFormattedLocalStorage.length > 0) {
      setItemsInCarts(itemsInCartsFormattedLocalStorage);
    }
  }, []);

  return (
    <CartContext.Provider value={{ itemsInCarts, setItemsInCarts }}>
      {children}
    </CartContext.Provider>
  );
};

export function useCart(): CartContextType {
  const context = useContext(CartContext);
  if (context === undefined) {
    throw new Error('useCart deve ser usado dentro do CartProvider');
  }
  return context;
}
