import { FC, ReactNode } from 'react';

import { CartProvider } from 'context/useCart';

interface AppProviderProps {
  children: ReactNode;
}

export const AppProvider: FC<AppProviderProps> = ({ children }) => (
  <CartProvider>{children}</CartProvider>
);
