import axios from 'axios';

const api = axios.create({
  baseURL: 'http://gateway.marvel.com/',
  params: {
    ts: 1,
    apikey: 'f2f3b519f72e02ebd3eaa2a577c85984',
    hash: '249c69ab5757f6b4c5ff1d19164727fd',
  },
});

export { api };
