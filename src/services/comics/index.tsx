import { api } from 'services';
import { ParamsGetComicsType } from 'types';

interface ResponseType {
  data: { data: { [key: string]: undefined } };
}

interface ResponseComicDetailsType {
  data: { data: { results: { [key: string]: undefined } } };
}

export const comics = {
  getComics: async (params: ParamsGetComicsType): Promise<ResponseType> => {
    return await api.get('/v1/public/comics', { params });
  },

  getComicDetails: async (
    comicId: number,
  ): Promise<ResponseComicDetailsType> => {
    return await api.get(`/v1/public/comics/${comicId}`);
  },
};
