import { useEffect } from 'react';
import { useCart } from 'context/useCart';
import { useComics } from 'hooks/useComics';
import { Loading, Button } from 'components';
import { storage } from 'utils/local-storage';
import { enumRoutes } from 'routes/enumRoutes';
import { ComicDetailsType, CartItem } from 'types';
import { toPriceRS, formatDate } from 'utils/converters';
import { useParams, useNavigate } from 'react-router-dom';

const ComicDetails = () => {
  const { itemsInCarts, setItemsInCarts } = useCart();

  const navigate = useNavigate();
  const { comicId } = useParams<{ comicId: string }>();
  const { getComicDetails, comicDetails, isLoadingComicDetails } = useComics();

  useEffect(() => {
    if (comicId) {
      getComicDetails(Number(comicId));
    }
  }, [getComicDetails, comicId]);

  const resourceURI = comicDetails?.thumbnail
    ? `${comicDetails.thumbnail.path}.${comicDetails.thumbnail.extension}`
    : '';

  const handleCheckoutComic = async () => {
    const newComic: CartItem = {
      count: 1,
      comicId: Number(comicId),
      price: price,
      resourceURI: resourceURI,
      name: comicDetails?.title || 'Unknown Title',
    };
    try {
      const idExistsIndex: number | null = itemsInCarts.findIndex(
        (item: CartItem) => item?.comicId === comicId,
      );
      if (idExistsIndex !== null && idExistsIndex !== -1) {
        const newArray = [...itemsInCarts];
        if (newArray[idExistsIndex].count !== undefined) {
          newArray[idExistsIndex].count! += 1;
        }
        setItemsInCarts(newArray);
      } else {
        setItemsInCarts((old: CartItem[]) => [...old, newComic]);
      }
      const updatedCart =
        idExistsIndex !== null && idExistsIndex !== -1
          ? itemsInCarts
          : [...itemsInCarts, newComic];
      localStorage.setItem(storage.checkout, JSON.stringify(updatedCart));
      navigate(`${enumRoutes.COMICS_CHEACKOUT}`);
    } catch (error) {
      console.error('Erro ao processar o checkout:', error);
    }
  };

  const goBack = () => {
    navigate(`${enumRoutes.COMICS_LIST}`);
  };

  const inker = getCreatorName(comicDetails, 'inker');
  const writer = getCreatorName(comicDetails, 'writer');
  const date = getComicDate(comicDetails, 'onsaleDate');
  const price = getComicPrice(comicDetails, 'printPrice');
  const cover = getCreatorName(comicDetails, 'penciler (cover)');

  return (
    <div className="flex h-full w-full overflow-x-auto bg-slate-100 p-4">
      {isLoadingComicDetails && <Loading />}
      {!isLoadingComicDetails && comicDetails?.id && (
        <div className="flex w-full flex-col gap-2">
          <div className="flex flex-row items-center gap-2">
            <button onClick={goBack} className="hover:opacity-50">
              <span className="text-xs text-[#333333]">Voltar</span>
            </button>
            <div className="h-3 w-[1px] bg-slate-300" />
            <span
              className="text-xs font-bold text-[#ff171f]"
              title={comicDetails?.title}
            >
              Detalhes do quadrinho
            </span>
          </div>
          <div className="grid h-full w-full grid-cols-3 rounded-sm bg-white p-4 shadow-md">
            <div className="h-full w-[300px]" title={comicDetails?.title}>
              <img
                src={resourceURI}
                alt="Comic Cover"
                className="h-full w-full shadow-sm"
              />
            </div>
            <div className="flex w-full justify-center">
              <div className="flex w-full flex-col items-start justify-start gap-6">
                <span className="text-xl font-medium text-[#333333]">
                  {comicDetails?.title}
                </span>
                <div>
                  <div className="flex flex-row items-start">
                    <span className="text-4xl font-light text-[#333333]">
                      {toPriceRS(price)}
                    </span>
                  </div>
                  <div className="flex flex-row gap-1">
                    <span className="text-sm font-light text-[#333333]">
                      em 5x
                    </span>
                    <div className="flex flex-row items-start">
                      <span className="text-sm font-light text-[#333333]">
                        {toPriceRS(price, 5)} sem juros
                      </span>
                    </div>
                  </div>
                  <button
                    className="hover:opacity-50"
                    onClick={() =>
                      alert('Esta função está em desenvolvimento.')
                    }
                  >
                    <span className="text-xs font-medium text-[#ff171f]">
                      ver os meios de pagamento
                    </span>
                  </button>
                </div>
                <div className="flex flex-col items-start gap-2">
                  <span className="text-xs font-medium text-[#333333]">
                    O que você precisa saber sobre este produto
                  </span>
                  <li className="text-xs text-[#333333]">
                    Publicados: {date ? formatDate(date) : '-'}
                  </li>
                  <li className="text-xs text-[#333333]">
                    Escritor: {writer || 'Desconhecido'}
                  </li>
                  <li className="text-xs text-[#333333]">
                    Lápis: {inker || 'Desconhecido'}
                  </li>
                  <li className="text-xs text-[#333333]">
                    Artista da capa: {cover || 'Desconhecido'}
                  </li>
                </div>
              </div>
            </div>
            <div className="justify- flex w-full justify-end">
              <div className="flex w-[300px] flex-col gap-6 rounded-md border border-solid border-gray-200 p-4">
                <div className="flex flex-col items-start">
                  <span className="text-xs font-bold text-[#333333]">
                    Frete grátis
                  </span>
                  <span className="text-xs text-[#333333]">
                    Saiba os prazos de entrega e as formas de envio.
                  </span>
                  <button
                    className="hover:opacity-50"
                    onClick={() =>
                      alert('Esta função está em desenvolvimento.')
                    }
                  >
                    <span className="text-xs font-medium text-[#ff171f]">
                      Calcular o prazo de entrega
                    </span>
                  </button>
                </div>
                <div className="flex flex-col">
                  <span className="text-xs font-medium text-[#333333]">
                    Loja oficial <b className="text-[#ff171f]">Marvel comics</b>
                  </span>
                  <span className="text-xs text-[#333333]">+ 1000 vendas</span>
                </div>
                <div className="flex flex-col gap-2">
                  <Button onClick={handleCheckoutComic}>Comprar agora</Button>
                  <Button onClick={handleCheckoutComic} variant="outline">
                    Adicionar ao carrinho
                  </Button>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

const getCreatorName = (comicDetails: ComicDetailsType, role: string) => {
  if (comicDetails?.creators?.items)
    return comicDetails?.creators?.items
      .filter((item: { role?: string }) => item.role === role)
      .map((item: { name?: string }) => item.name)[0];
};

const getComicPrice = (comicDetails: ComicDetailsType, type: string) => {
  return comicDetails?.prices
    ?.filter((item: { type?: string }) => item.type === type)
    .map((item: { price?: number }) => item.price)[0];
};

const getComicDate = (comicDetails: ComicDetailsType, type: string) => {
  return comicDetails?.dates
    ?.filter((item: { type?: string }) => item.type === type)
    .map((item: { date?: string }) => item.date)[0];
};

export { ComicDetails };
