import { CartItem } from 'types';
import { Icon, Button } from 'components';
import { useCart } from 'context/useCart';
import { toPriceRS } from 'utils/converters';
import { storage } from 'utils/local-storage';
import { useNavigate } from 'react-router-dom';
import { enumRoutes } from 'routes/enumRoutes';
import IconButton from '@mui/material/IconButton';

const ComicsCheckout = () => {
  const navigate = useNavigate();
  const { itemsInCarts, setItemsInCarts } = useCart();

  const gotoBackAsShopping = () => {
    navigate(`${enumRoutes.COMICS_LIST}`);
  };

  const handleDeleteComic = (comicId: number) => {
    const newArray = itemsInCarts?.filter((item) => item.comicId !== comicId);
    setItemsInCarts(newArray || []);
    localStorage.setItem(storage.checkout, JSON.stringify(newArray));
  };

  const handleIncreaseNumberOfComics = (comicId: number) => {
    const newArray = (itemsInCarts || []).map((item) => {
      if (item.comicId === comicId) {
        return {
          ...item,
          count: (item.count || 1) + 1,
        };
      } else {
        return item;
      }
    });
    setItemsInCarts(newArray);
    localStorage.setItem(storage.checkout, JSON.stringify(newArray));
  };

  const handleDecreaseAmountOfComic = (comicId: number, count: number) => {
    if (count > 1) {
      const newArray = (itemsInCarts || []).map((item: CartItem) => {
        if (item.comicId === comicId) {
          return {
            ...item,
            count: (item.count || 1) - 1,
          };
        } else {
          return item;
        }
      });
      setItemsInCarts(newArray);
      localStorage.setItem(storage.checkout, JSON.stringify(newArray));
    } else {
      handleDeleteComic(comicId);
    }
  };

  return (
    <div className="flex h-full w-full overflow-x-auto bg-slate-100 p-4">
      {itemsInCarts?.length > 0 && (
        <div className="flex w-full flex-col gap-4">
          <div>
            <span className="text-xl text-gray-400">
              <b className="text-[#333333]">Carrinho</b> Clique em finalizar
              compra para efetuar o seu pedido.
            </span>
          </div>
          <div className="w-full rounded-sm bg-white p-4 shadow-md">
            <div className="w-full">
              <table className="w-full overflow-hidden border border-solid border-gray-200">
                <thead className="bg-white-20 border border-solid border-gray-200 bg-slate-100">
                  <tr>
                    <th className="text-neutral_variant-30 w-auto border border-solid border-gray-200 p-2.5 text-left text-xs font-bold">
                      Produto
                    </th>
                    <th className="w-auto border border-solid border-gray-200 p-2.5 text-center text-xs font-bold">
                      Preço unitário
                    </th>
                    <th className="w-auto border border-solid border-gray-200 p-2.5 text-center text-xs font-bold">
                      Quantidade
                    </th>
                    <th className="w-auto border border-solid border-gray-200 p-2.5 text-center text-xs font-bold">
                      Subtotal
                    </th>
                    <th className="w-auto border border-solid border-gray-200 p-2.5 text-center text-xs font-bold">
                      Excluir
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {itemsInCarts?.map((item: CartItem) => {
                    let subtotal = 0;
                    if (item?.price && item?.count) {
                      subtotal = item?.price * item?.count;
                    }
                    return (
                      <tr key={item?.comicId}>
                        <td className="w-auto border border-solid border-gray-200 p-2">
                          <div className="flex flex-row justify-start gap-2">
                            <div className="h-[120px] w-[90px] bg-black">
                              <img
                                src={item?.resourceURI}
                                className="h-full w-full"
                                alt={item?.name}
                              />
                            </div>
                            <div className="flex flex-col">
                              <span className="text-xs font-bold">
                                Quadrinho
                              </span>
                              <span className="text-xs">{item?.name}</span>
                            </div>
                          </div>
                        </td>
                        <td className="w-auto border border-solid border-gray-200 p-2 text-center text-base font-bold">
                          {toPriceRS(item?.price)}
                        </td>
                        <td className="w-auto border border-solid border-gray-200 p-2 text-center">
                          <div className="flex flex-row items-center justify-center gap-1">
                            <IconButton
                              onClick={() =>
                                handleDecreaseAmountOfComic(
                                  item?.comicId || 0,
                                  item?.count || 0,
                                )
                              }
                            >
                              <Icon name="minus" />
                            </IconButton>
                            <input
                              disabled
                              value={item?.count}
                              className="h-8 w-10 rounded-sm border border-solid border-gray-200 text-center"
                            />
                            <IconButton
                              onClick={() =>
                                handleIncreaseNumberOfComics(item?.comicId || 0)
                              }
                            >
                              <Icon name="plus" />
                            </IconButton>
                          </div>
                        </td>
                        <td className="w-auto border border-solid border-gray-200 p-2 text-center text-base font-bold">
                          {toPriceRS(subtotal)}
                        </td>
                        <td className="w-auto border border-solid border-gray-200 p-2 text-center">
                          <IconButton
                            onClick={() =>
                              handleDeleteComic(item?.comicId || 0)
                            }
                          >
                            <Icon name="delete" />
                          </IconButton>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>
          <div className="flex w-full flex-row justify-end gap-4 pb-4">
            <div className="w-[300px]">
              <Button onClick={gotoBackAsShopping} variant="outline">
                Continuar comprando
              </Button>
            </div>
            <div className="w-[300px]">
              <Button
                onClick={() => alert('Esta função está em desenvolvimento.')}
              >
                Finalizar compra
              </Button>
            </div>
          </div>
        </div>
      )}
      {itemsInCarts?.length === 0 && (
        <div className="flex h-full w-full flex-col items-center justify-center gap-4">
          <span className="text-3xl font-medium text-[#333333]">
            Não existem produtos no carrinho
          </span>
          <button
            onClick={gotoBackAsShopping}
            className="h-10 rounded-md bg-[#ff171f] px-6 font-medium text-white hover:opacity-50"
          >
            Ir às compras
          </button>
        </div>
      )}
    </div>
  );
};

export { ComicsCheckout };
