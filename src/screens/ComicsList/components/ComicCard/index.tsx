import { FC, useEffect, useState, useRef, MouseEvent } from 'react';
import { toPriceRS } from 'utils/converters';

interface ComicCardProps {
  price?: number;
  title?: string;
  comicId?: number;
  resourceURI?: string;
  handleComicDetails: (comicId: number) => void;
}

const ComicCard: FC<ComicCardProps> = ({
  price,
  title,
  comicId,
  resourceURI = '',
  handleComicDetails,
}) => {
  const cardRef = useRef<HTMLDivElement | null>(null);
  const [showButtons, setShowButtons] = useState(false);

  useEffect(() => {
    const setHeight = () => {
      if (cardRef.current) {
        const width = cardRef.current.offsetWidth;
        cardRef.current.style.height = `${width}px`;
      }
    };
    window.addEventListener('resize', setHeight);
    setHeight();
    return () => {
      window.removeEventListener('resize', setHeight);
    };
  }, []);

  const handleMouseEnter = () => {
    setShowButtons(true);
  };

  const handleMouseLeave = () => {
    setShowButtons(false);
  };

  const handleClick = (event: MouseEvent<HTMLDivElement>) => {
    event.preventDefault();
    handleComicDetails(comicId);
  };

  return (
    <div
      title={title}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
      onClick={handleClick}
      className={`flex h-full cursor-pointer flex-col items-center justify-start gap-2 rounded-sm bg-white py-6 ${
        showButtons && 'shadow-lg'
      }`}
    >
      <div
        ref={cardRef}
        className="flex w-full items-center justify-center px-7"
      >
        <img
          alt={title}
          src={resourceURI}
          className="h-full w-full shadow-sm"
        />
      </div>
      <div className="my-2 h-[1px] w-full bg-gray-200" />
      <div className="flex flex-col gap-2 px-4">
        <div className="flex flex-col">
          <div className="flex flex-row items-center gap-1">
            <span className="text-2xl font-normal text-[#333333]">
              {toPriceRS(price)}
            </span>
            <span className="text-xs text-[#ff171f]">15% OFF</span>
          </div>
          <span className="text-xs text-[#ff171f]">
            5x {toPriceRS(price, 5)} sem juros
          </span>
          <span className="text-xs font-bold text-[#ff171f]">Frete grátis</span>
        </div>
        <span className="line-clamp-2 text-ellipsis text-sm text-[#333333]">
          {title}
        </span>
      </div>
    </div>
  );
};

export { ComicCard };
