import { useState, useEffect, ChangeEvent, useRef } from 'react';

import { ResultsType } from 'types';
import { ComicCard } from './components';
import { useComics } from 'hooks/useComics';
import { enumRoutes } from 'routes/enumRoutes';
import { useNavigate } from 'react-router-dom';
import { Pagination, Loading } from 'components';

const PAGE_SIZE = 24;

const ComicsList = () => {
  const navigate = useNavigate();
  const scrollContentRef = useRef(null);
  const { getComics, listOfComics, isLoadingComics } = useComics();

  const [filter, setFilter] = useState({
    page: 1,
    pageSize: PAGE_SIZE,
  });

  useEffect(() => {
    const params = {
      offset: filter.page - 1,
      limit: filter.pageSize,
    };
    getComics(params);
  }, [getComics, filter]);

  const handleChangePage = (
    _: ChangeEvent<unknown> | null,
    newPage: number,
  ) => {
    setFilter((old) => ({ ...old, page: newPage }));
    if (scrollContentRef && scrollContentRef.current) {
      (scrollContentRef.current as any).scrollTop = 0;
    }
  };

  const handleComicDetails = (comicId: number) => {
    navigate(
      `${enumRoutes.COMIC_DETAILS.replace(':comicId', String(comicId))}`,
    );
  };

  const handleChangeRowsPerPage = (event: ChangeEvent<{ value: unknown }>) => {
    const newPageSize = event.target.value as number;
    setFilter((old) => ({
      ...old,
      page: 1,
      pageSize: newPageSize || PAGE_SIZE,
    }));
    if (scrollContentRef && scrollContentRef.current) {
      (scrollContentRef.current as any).scrollTop = 0;
    }
  };

  return (
    <div
      ref={scrollContentRef}
      className="flex h-full w-full items-start justify-center overflow-x-auto bg-slate-100 p-4"
    >
      <div className="flex h-full w-full flex-col gap-4">
        {isLoadingComics && <Loading />}
        {!isLoadingComics && (listOfComics?.results?.length || 0) > 0 && (
          <div className="grid w-full auto-cols-auto grid-cols-2 items-start gap-4 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-6">
            {listOfComics?.results?.map((item: ResultsType) => {
              let resourceURI = '';
              if (item?.thumbnail?.path && item?.thumbnail?.extension) {
                resourceURI = `${item.thumbnail.path}.${item.thumbnail.extension}`;
              }
              return (
                <div key={item.id} className="h-full w-full">
                  <ComicCard
                    title={item.title}
                    comicId={item.id}
                    price={item.prices && item.prices[0].price}
                    handleComicDetails={handleComicDetails}
                    resourceURI={resourceURI}
                  />
                </div>
              );
            })}
          </div>
        )}
        {!isLoadingComics && (listOfComics?.results?.length || 0) > 0 && (
          <div className="pb-4">
            <Pagination
              count={Math.ceil((listOfComics.total || 0) / filter.pageSize)}
              page={filter.page}
              rowsPerPage={filter.pageSize}
              changePerPage={handleChangeRowsPerPage}
              onChange={handleChangePage}
            />
          </div>
        )}
      </div>
    </div>
  );
};

export { ComicsList };
