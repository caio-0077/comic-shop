import CircularProgress from '@mui/material/CircularProgress';

export const Loading = () => {
  return (
    <div className="flex h-full w-full flex-row items-center justify-center p-20">
      <div className="flex gap-5 rounded-sm bg-white p-5 shadow-sm">
        <span className="text-sm">Carregando</span>
        <CircularProgress style={{ color: '#ff171f' }} size={20} />
      </div>
    </div>
  );
};
