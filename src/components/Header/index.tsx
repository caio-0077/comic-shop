import { Link } from 'react-router-dom';

import { Icon } from 'components';
import { useCart } from 'context/useCart';
import { enumRoutes } from 'routes/enumRoutes';
import { useNavigate } from 'react-router-dom';
import marvelLogo from 'assets/marvel-logo.png';
import { IconButton, Badge } from '@mui/material';

const Header = () => {
  const navigate = useNavigate();
  const { itemsInCarts } = useCart();
  const quantityItemsInCart = itemsInCarts.length;

  const goToHome = () => {
    navigate(`${enumRoutes.COMICS_LIST}`);
  };

  const goToCart = () => {
    navigate(`${enumRoutes.COMICS_CHEACKOUT}`);
  };

  return (
    <div className="flex h-14 w-screen items-center justify-center bg-[#202020] px-4">
      <div className="flex h-14 w-full max-w-[1366px] items-center justify-between px-4">
        <IconButton onClick={goToHome}>
          <Icon name="home" className="text-white" />
        </IconButton>
        <Link
          to={enumRoutes.COMICS_LIST}
          className="flex items-center justify-center"
        >
          <img
            src={marvelLogo}
            alt="marvel-logo"
            className="h-[50px] w-[115px]"
          />
        </Link>
        <IconButton onClick={goToCart}>
          <Badge badgeContent={quantityItemsInCart} color="error" max={99}>
            <Icon name="shopping-cart" className="text-white" />
          </Badge>
        </IconButton>
      </div>
    </div>
  );
};
export { Header };
