import { FC } from 'react';
import AddIcon from '@mui/icons-material/Add';
import HomeIcon from '@mui/icons-material/Home';
import DeleteIcon from '@mui/icons-material/Delete';
import RemoveIcon from '@mui/icons-material/Remove';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';

interface IconProps {
  name: string;
  className?: string;
  size?: 'inherit' | 'large' | 'medium' | 'small';
}

const Icon: FC<IconProps> = ({ name, size = 'medium', className }) => {
  const Icons = {
    plus: <AddIcon fontSize={size} className={className} />,
    home: <HomeIcon fontSize={size} className={className} />,
    delete: <DeleteIcon fontSize={size} className={className} />,
    minus: <RemoveIcon fontSize={size} className={className} />,
    'shopping-cart': <ShoppingCartIcon fontSize={size} className={className} />,
  }[name];
  return Icons || null;
};

export { Icon };
