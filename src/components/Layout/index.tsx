import { Content } from './Content';
import { Container } from './Container';

export const Layout = {
  Content: Content,
  Container: Container,
};
