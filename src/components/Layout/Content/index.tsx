import { FC, ReactNode } from 'react';

interface ContentProps {
  children: ReactNode;
}

const Content: FC<ContentProps> = ({ children }) => {
  return (
    <div className="flex h-[calc(100vh-56px)] w-full max-w-[1366px] justify-center overflow-hidden">
      {children}
    </div>
  );
};

export { Content };
