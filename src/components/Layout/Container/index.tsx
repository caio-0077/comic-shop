import { FC, ReactNode } from 'react';

interface ContainerProps {
  children: ReactNode;
}

const Container: FC<ContainerProps> = ({ children }) => {
  return (
    <div className="flex h-screen w-screen flex-col items-center justify-center overflow-hidden bg-slate-100">
      {children}
    </div>
  );
};

export { Container };
