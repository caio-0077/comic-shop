import { ChangeEvent, FC } from 'react';

import { styled } from '@mui/system';
import { Pagination as PaginationMUI } from '@mui/material';

interface PaginationProps {
  page?: number;
  onChange?: (_event: ChangeEvent<unknown>, _page: number) => void;
  rowsPerPage?: number;
  count: number;
  changePerPage?: (_event: ChangeEvent<HTMLSelectElement>) => void;
  rowsPerPagePersonality?: number[];
}

const CustomizedPagination = styled(PaginationMUI)`
  .MuiPaginationItem-root.Mui-selected {
    background-color: #ff171f;
    color: #fff;
  }

  .MuiPaginationItem-page:hover {
    background-color: rgba(152, 152, 152, 0.08);
  }
`;

export const Pagination: FC<PaginationProps> = ({
  page,
  onChange,
  rowsPerPage,
  count,
  changePerPage,
  rowsPerPagePersonality = [24, 36, 48],
}) => {
  return (
    <div className="xs:flex-col flex flex-row items-center justify-center gap-3">
      <CustomizedPagination
        page={page}
        shape="rounded"
        defaultPage={1}
        siblingCount={1}
        onChange={onChange}
        count={Math.ceil(count)}
      />
      <div className="cursor-pointer">
        <select
          value={rowsPerPage}
          onChange={changePerPage}
          className="focus:ring-green-dark focus:border-green-dark h-8 rounded-md bg-white px-2 shadow-sm focus:outline-none sm:text-sm"
        >
          {rowsPerPagePersonality.map((item, index) => (
            <option key={index} value={item}>
              {item}
            </option>
          ))}
        </select>
      </div>
    </div>
  );
};
