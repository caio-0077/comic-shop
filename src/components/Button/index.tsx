import { FC, ReactNode } from 'react';

const variants = {
  solid:
    'h-10 w-full rounded-md bg-[#ff171f] font-medium text-white hover:opacity-60',
  outline:
    'h-10 w-full rounded-md bg-[#ff171f] bg-opacity-10 font-bold text-[#ff171f] hover:opacity-60',
};

interface ButtonProps {
  children?: ReactNode;
  onClick?: () => void;
  variant?: 'solid' | 'outline';
}

const Button: FC<ButtonProps> = ({ children, onClick, variant = 'solid' }) => {
  return (
    <button onClick={onClick} className={variants[variant]}>
      {children}
    </button>
  );
};

export { Button };
