import { StrictMode } from 'react';
import { router } from 'routes';
import { RouterProvider } from 'react-router-dom';

const App = () => {
  return (
    <StrictMode>
      <RouterProvider router={router} />
    </StrictMode>
  );
};

export { App };
