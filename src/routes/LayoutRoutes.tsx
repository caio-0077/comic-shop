import { Suspense } from 'react';
import { Layout, Header } from 'components';
import { Outlet, Await, useLoaderData } from 'react-router-dom';

const LayoutRoutes = () => {
  const data = useLoaderData();

  return (
    <Suspense>
      <Await resolve={data}>
        <Layout.Container>
          <Header />
          <Layout.Content>
            <Outlet />
          </Layout.Content>
        </Layout.Container>
      </Await>
    </Suspense>
  );
};
export { LayoutRoutes };
