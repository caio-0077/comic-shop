import { enumRoutes } from './enumRoutes';
import { LayoutRoutes } from './LayoutRoutes';
import { createBrowserRouter } from 'react-router-dom';
import { ComicsCheckout, ComicDetails, ComicsList } from 'screens';

export const router = createBrowserRouter([
  {
    children: [
      {
        path: '/',
        element: <LayoutRoutes />,
        children: [
          {
            path: enumRoutes.COMICS_LIST,
            element: <ComicsList />,
          },
          {
            path: enumRoutes.COMIC_DETAILS,
            element: <ComicDetails />,
          },
          {
            path: enumRoutes.COMICS_CHEACKOUT,
            element: <ComicsCheckout />,
          },
        ],
      },
    ],
  },
]);
