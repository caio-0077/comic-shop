export const enumRoutes = {
  COMICS_LIST: '/',
  COMIC_DETAILS: '/quadrinho/detalhes/:comicId',
  COMICS_CHEACKOUT: '/quadrinhos/checkout',
};
