export type ParamsGetComicsType = {
  offset?: number;
  limit?: number;
};

export interface CartItem {
  count?: number | undefined;
  comicId?: number | undefined;
  price?: number | undefined;
  resourceURI?: string | undefined;
  name?: string | undefined;
}

type PricesType = {
  type?: string;
  price?: number;
};

type DatesType = {
  type?: string;
  date?: string;
};

type ItemsType = {
  role?: string;
  name?: string;
};

export type ComicDetailsType = {
  id?: number | undefined;
  title?: string | undefined;
  prices?: PricesType[];
  dates?: DatesType[];
  creators?: {
    items?: ItemsType[];
  };
  items?: number | undefined;
  thumbnail?: {
    path?: string;
    extension?: string;
  };
};

export type ResultsType = {
  title?: string;
  id?: number;
  prices?: PricesType[];
  thumbnail?: {
    path?: string | undefined;
    extension?: string | undefined;
  };
};

export type ComicsType = {
  total?: number | undefined;
  results?: ResultsType[];
};
