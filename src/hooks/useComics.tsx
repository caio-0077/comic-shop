import { useState, useCallback } from 'react';

import { comics } from 'services/comics';
import { ParamsGetComicsType, ComicDetailsType, ComicsType } from 'types';

export const useComics = () => {
  const [isLoadingComics, setIsLoadingComics] = useState(false);
  const [listOfComics, setListOfComics] = useState<ComicsType>({});
  const [comicDetails, setComicDetails] = useState<ComicDetailsType>({});
  const [isLoadingComicDetails, setIsLoadingComicDetails] = useState(false);

  const getComics = useCallback(async (params: ParamsGetComicsType) => {
    try {
      setIsLoadingComics(true);
      const response = await comics.getComics(params);
      setListOfComics(response.data.data);
    } catch (error) {
    } finally {
      setIsLoadingComics(false);
    }
  }, []);

  const getComicDetails = useCallback(async (comicId: number) => {
    try {
      setIsLoadingComicDetails(true);
      const response = await comics.getComicDetails(comicId);
      if (response.data.data.results[0]) {
        setComicDetails(response.data.data.results[0]);
      }
    } catch (error) {
    } finally {
      setIsLoadingComicDetails(false);
    }
  }, []);

  return {
    getComics,
    listOfComics,
    isLoadingComics,
    getComicDetails,
    comicDetails,
    isLoadingComicDetails,
  };
};
