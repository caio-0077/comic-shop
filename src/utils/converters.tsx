import { format, addHours, isValid } from 'date-fns';

type DateFormat = 'date' | 'dateTime' | 'dateName';

const toPriceRS = (val: number | string = '0', divide: number = 1): string => {
  const numericValue = typeof val === 'string' ? parseFloat(val) : Number(val);
  const value = (numericValue * 5.3) / divide;

  if (isNaN(value)) {
    throw new Error('O valor não é um número válido.');
  }

  return new Intl.NumberFormat('pt-BR', {
    style: 'currency',
    currency: 'BRL',
    minimumFractionDigits: 2,
    maximumFractionDigits: 2,
  }).format(value);
};

const formatDate = (
  dateString: string | undefined,
  _format?: DateFormat,
): string => {
  if (!dateString) return '';
  dateString = dateString.replace(/\//g, '-');

  const dateFormat = _format || 'date';

  const formats: Record<DateFormat, string> = {
    date: 'dd/MM/yyyy',
    dateTime: 'dd/MM/yyyy HH:mm:ss',
    dateName: 'dd MMMM yyyy',
  };

  if (!dateString || dateString.indexOf('0000') >= 0) {
    return format(new Date(), formats[dateFormat]);
  }

  const isDatePTBR = dateString.indexOf('-') === -1;

  if (isDatePTBR) return dateString.substring(0, 10);

  const date = new Date(dateString);

  if (!isValid(date)) {
    return '';
  }

  const dateTimeZone = addHours(date, 3);
  const formattedDate = format(dateTimeZone, formats[dateFormat]);

  return formattedDate;
};

export { toPriceRS, formatDate };
