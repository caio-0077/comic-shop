const LOCAL_GLOBAL = '@MarvelComics';

const storage = {
  checkout: `${LOCAL_GLOBAL}:checkout`,
};

const resetStorages = () => {
  localStorage.removeItem(storage.checkout);
};

export { LOCAL_GLOBAL, storage, resetStorages };
